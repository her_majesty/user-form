import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appModalPlaceholder]'
})
export class ModalPlaceholderDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}

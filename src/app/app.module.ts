import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UserFormComponent } from './components/user-form/user-form.component';
import { ReactiveFormsModule } from "@angular/forms";
import { AlertModalComponent } from './components/alert-modal/alert-modal.component';
import { ModalPlaceholderDirective } from './directives/modal-placeholder.directive';

@NgModule({
  declarations: [
    AppComponent,
    UserFormComponent,
    AlertModalComponent,
    ModalPlaceholderDirective
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AlertModalComponent]
})
export class AppModule { }

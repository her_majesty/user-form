import {Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FIELDS_VALIDATION } from 'src/app/constants/fields-validation';
import { repeatingSymbols } from 'src/app/validators/repeating-symbols.validator';
import { repeatingValues } from 'src/app/validators/repeating-values.validator';
import { totalValueLength } from 'src/app/validators/total-value-length.validator';
import {AlertModalComponent} from "../alert-modal/alert-modal.component";
import {ModalPlaceholderDirective} from "../../directives/modal-placeholder.directive";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit, OnDestroy {
  userForm: FormGroup;
  @ViewChild (ModalPlaceholderDirective, {static: false}) modalHost: ModalPlaceholderDirective;
  private hostViewContainerRef: ViewContainerRef;
  private modalCloseSubscription: Subscription;

  get name () {
    return this.userForm.get('name');
  }

  get lastname () {
    return this.userForm.get('lastname');
  }

  get patronymic () {
    return this.userForm.get('patronymic');
  }

  constructor (private componentFactoryResolver: ComponentFactoryResolver) {

  }

  ngOnInit() {
    this.userForm = new FormGroup({
      name: new FormControl('',
        [Validators.pattern(FIELDS_VALIDATION.name.pattern),
          Validators.required,
          Validators.minLength(FIELDS_VALIDATION.name.minLength),
          Validators.maxLength(FIELDS_VALIDATION.name.maxLength),
          repeatingSymbols]),
      lastname: new FormControl('',
        [Validators.pattern(FIELDS_VALIDATION.lastname.pattern),
          Validators.required,
          Validators.minLength(FIELDS_VALIDATION.lastname.minLength),
          Validators.maxLength(FIELDS_VALIDATION.lastname.maxLength),
          repeatingSymbols]),
      patronymic: new FormControl('',
        [Validators.pattern(FIELDS_VALIDATION.patronymic.pattern),
          Validators.required,
          Validators.minLength(FIELDS_VALIDATION.patronymic.minLength),
          Validators.maxLength(FIELDS_VALIDATION.patronymic.maxLength),
          repeatingSymbols]),
    }, [repeatingValues, totalValueLength]);
  }

  ngAfterViewInit() {
    this.hostViewContainerRef = this.modalHost.viewContainerRef;
  }

  ngOnDestroy() {
    this.modalCloseSubscription.unsubscribe();
  }

  capitalize($event) {
    $event.target.value = $event.target.value.charAt(0).toUpperCase() + $event.target.value.slice(1);
  }

  openModal (message: string) {
    const alertModalComponentFactory = this.componentFactoryResolver.resolveComponentFactory(AlertModalComponent);
    this.hostViewContainerRef.clear();
    const alertModalComponentRef = this.hostViewContainerRef.createComponent(alertModalComponentFactory);
    alertModalComponentRef.instance.message = message;
    this.modalCloseSubscription = alertModalComponentRef.instance.close.subscribe(() => this.closeModal())
  }

  closeModal() {
    this.hostViewContainerRef.clear();
  }

  onSubmit() {
    Object.keys(this.userForm.controls).forEach(controlName => this[controlName].markAsTouched());
    if (this.userForm.invalid) {
      this.openModal('wrong password');
      return;
    }
    console.log('Form is submitted!', this.userForm);
  }
}

export const FIELDS_VALIDATION = {
  name: {
    minLength: 2,
    maxLength: 25,
    pattern: new RegExp('^[А-ЩЬЮЯҐЄІЇа-щьюяґєії]+$')
  },
  lastname: {
    minLength: 2,
    maxLength: 25,
    pattern: new RegExp('^[А-ЩЬЮЯҐЄІЇа-щьюяґєії]+$')
  },
  patronymic: {
    minLength: 2,
    maxLength: 25,
    pattern: new RegExp('^[А-ЩЬЮЯҐЄІЇа-щьюяґєії]+$')
  },
}

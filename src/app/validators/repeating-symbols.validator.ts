import {AbstractControl} from "@angular/forms";

export function repeatingSymbols (control: AbstractControl): {[key: string]: any} | null {

  let hasRepeatingSymbols = false;

  control.value && control.value.split('').reduce((prev, current) => {
      if (prev.toLowerCase() === current.toLowerCase()) {
        hasRepeatingSymbols = true;
      }
      return current;
  } )

  return hasRepeatingSymbols ? { repeatingSymbols: true }: null;
}

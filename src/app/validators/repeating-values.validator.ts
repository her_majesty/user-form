import {FormGroup} from "@angular/forms";

export function repeatingValues (form: FormGroup): {[key: string]: any} | null {

  let notEmptyValues = Object.values(form.controls).map(control => control.value.toLowerCase()).filter(Boolean);
  let uniqueValues = new Set(notEmptyValues);

  return notEmptyValues.length === uniqueValues.size ?
      null: 
    { repeatingValues: true };
}

import {FormGroup} from "@angular/forms";

export function totalValueLength (form: FormGroup): {[key: string]: any} | null {
  let total = '';
  Object.keys(form.controls).forEach(key => {
    total += form.controls[key].value;
  });
    return total.length > 50? {totalValueLength: true} : null;
}
